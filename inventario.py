inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    inventario[codigo] = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    return inventario


def listar(inventario: dict):
    for i in inventario:
        valores = inventario[i]
        print(f'{i}, {valores["nombre"]}, {valores["precio"]}, {valores["cantidad"]}')


def consultar(codigo: str, inventario: dict):
    if codigo in inventario:
        return inventario[codigo]
    else:
        return None


def listar_out_stock(inventario: dict):
    for i in inventario:
        if inventario[i]["cantidad"] == 0:
            return inventario[i]
    else:
        return ("no hay objetos out of stock.")


def main():
    while True:
        menu = input("1:para insertar producto\n2:para listar inventario\n3:para consultar producto\n4:para listar productos out of stock\n5:para salir\nelige una opcion: ")
        if menu == "1":
            insertar(input("dame el codigo del producto: "), input("dame el nobre del producto: "),
            float(input("dame el precio del producto: ")), int(input("dame la cantidad del producto: ")))
            print(inventario)
        elif menu == "2":
            print(listar(inventario))
        elif menu == "3":
            print(consultar(input("dame el codigo a consultar: "), inventario))
        elif menu == "4":
            print(listar_out_stock(inventario))
        elif menu == "5":
            break


if __name__ == '__main__':
    main()